package search

import java.io.File

class SearchEngine(dataPath: String) {
    private val people: MutableList<String>
    private val invertedIndex: MutableMap<String, MutableList<Int>> = mutableMapOf()

    init {
        val file = File(dataPath)
        val data = file.readText()
        people = data.split("\n").toMutableList()
        people.withIndex().forEach { personWithIndex ->
            val person = personWithIndex.value
            val index = personWithIndex.index
            val words = person.split("\\s+".toRegex()).map { it.lowercase() }
            words.forEach { word ->
                if (invertedIndex.contains(word)) invertedIndex[word]!!.add(index)
                else invertedIndex[word] = mutableListOf(index)
            }
        }
    }

    fun start() {
        while (true) {
            val option = getInput(
                "=== Menu ===\n1. Find a person\n2. Print all people\n0. Exit"
            ).toInt()
            when (option) {
                0 -> {
                    println("\nBye!")
                    break
                }

                1 -> searchPeople()

                2 -> println(people.joinToString("\n", "=== List of people ===\n", "\n"))

                else -> println("\nIncorrect option! Try again.")
            }
        }
    }

    private fun searchPeople() {
        val strategy = getInput("Select a matching strategy: ALL, ANY, NONE").uppercase()
        val queries = getInput("\nEnter a name or email to search all suitable people.").lowercase().split(" ")
        println(
            when (strategy) {
                "ALL" -> searchForAll(queries)

                "ANY" -> searchForAny(queries)

                "NONE" -> searchForNone(queries)

                else -> "No such strategy."
            }
        )
    }

    private fun searchForNone(queries: List<String>): String {
        val indices = (0..people.lastIndex).toMutableSet()
        queries.forEach { query ->
            invertedIndex[query]?.forEach { indices.remove(it) }
        }
        return if (indices.isEmpty()) "No matching found." else indices.joinToString(
            "\n",
            "${indices.size} ${if (indices.size == 1) "person" else "persons"} found:\n"
        ) { people[it] }
    }

    private fun searchForAny(queries: List<String>): String {
        val indices = mutableSetOf<Int>()
        queries.forEach { query ->
            invertedIndex[query]?.forEach { indices.add(it) }
        }
        return if (indices.isEmpty()) "No matching found." else indices.joinToString(
            "\n",
            "${indices.size} ${if (indices.size == 1) "person" else "persons"} found:\n"
        ) { people[it] }
    }

    private fun searchForAll(queries: List<String>): String {
        val indices = mutableSetOf<Int>()
        if (queries.all { invertedIndex.containsKey(it) }) {
            (0..people.lastIndex).forEach { index -> if (queries.all { invertedIndex[it]!!.contains(index) }) indices.add(index) }
        }
        return if (indices.isEmpty()) "No matching found." else indices.joinToString(
            "\n",
            "${indices.size} ${if (indices.size == 1) "person" else "persons"} found:\n"
        ) { people[it] }
    }

    /*private fun linearSearch(query: String) {
        val result = people.filter { it.replace(" ", "").lowercase().contains(query) }
        if (result.isEmpty()) println("No matching found.\n")
        else {
            println("${result.size} ${if (result.size == 1) "person" else "persons"} found:")
            println(result.joinToString("\n", postfix = "\n"))
        }
    }*/
}
