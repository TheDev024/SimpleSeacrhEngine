package search

import java.util.*

val scanner = Scanner(System.`in`)

fun getInput(message: String): String {
    println(message)
    return scanner.nextLine()
}

fun getOptions(message: String, options: Regex, errorMessage: String = ""): String {
    val option = getInput(message)
    return if (options.matches(option)) option else {
        if (errorMessage.isNotBlank()) println(errorMessage)
        getOptions(message, options, errorMessage)
    }
}

fun main(args: Array<String>) {
    SearchEngine(args[1]).start()
}
